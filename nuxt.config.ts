import type { NuxtConfig } from '@nuxt/types'
import buildModules from './plugins/config/build-modules'
import modules, { unpluginAutoImport, typescriptBuild } from './plugins/config/modules'
import head from './plugins/config/head'
import hooks from './plugins/config/hooks'
import loading from './plugins/config/loading'

const config: NuxtConfig = {
  // https://nuxtjs.org/docs/configuration-glossary/configuration-build
  build: {},
  // https://nuxtjs.org/docs/configuration-glossary/configuration-css
  css: [
    '~/assets/css/app.pcss'
  ],
  // https://nuxtjs.org/blog/moving-from-nuxtjs-dotenv-to-runtime-config
  publicRuntimeConfig: {
    baseURL: process.env.BASE_URL,
    apiURL: process.env.API_URL
  },
  privateRuntimeConfig: {},
  // Doc: https://go.nuxtjs.dev/config-components, github: https://github.com/nuxt/components
  components: true,
  // https://nuxtjs.org/docs/configuration-glossary/configuration-servermiddleware
  serverMiddleware: [],
  // https://nuxtjs.org/docs/configuration-glossary/configuration-head
  head,
  loading,
  // https://nuxtjs.org/docs/configuration-glossary/configuration-modules#buildmodules
  buildModules: [
    ['@nuxt/typescript-build', // https://typescript.nuxtjs.org
      typescriptBuild
    ],
    '@nuxtjs/composition-api/module', // https://composition-api.nuxtjs.org
    // 'unplugin-vue2-script-setup/nuxt', // https://github.com/antfu/unplugin-vue2-script-setup
    'nuxt-windicss', // https://windicss.org/integrations/nuxt.html
    '@nuxtjs/svg-sprite', // https://github.com/nuxt-community/svg-sprite-module
    ['unplugin-auto-import/nuxt', // https://github.com/antfu/unplugin-auto-import
      unpluginAutoImport
    ]
  ],
  ...buildModules,
  // https://nuxtjs.org/docs/configuration-glossary/configuration-modules/
  modules: [
    '@nuxtjs/axios', // https://axios.nuxtjs.org/setup
    '@nuxt/http',
    '@nuxt/content' // https://content.nuxtjs.org/
  ],
  ...modules,
  // https://nuxtjs.org/docs/configuration-glossary/configuration-plugins
  plugins: ['~/plugins/truncate'],
  // https://nuxtjs.org/docs/configuration-glossary/configuration-hooks
  hooks
}

export default config
