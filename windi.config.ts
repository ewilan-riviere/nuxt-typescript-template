import { defineConfig } from 'windicss/helpers'
import colors from 'windicss/colors'
import filtersPlugin from 'windicss/plugin/filters'
import formsPlugin from 'windicss/plugin/forms'
import aspectRatioPlugin from 'windicss/plugin/aspect-ratio'
import lineClampPlugin from 'windicss/plugin/line-clamp'
import typographyPlugin from 'windicss/plugin/typography'
import scrollbarPlugin from '@windicss/plugin-scrollbar'
import plugin from 'windicss/plugin'

export default defineConfig({
  darkMode: 'class', // or 'media'
  shortcuts: {
    'debug-screens': 'before:bottom-0 before:left-0 before:fixed before:z-[2147483647] before:px-1 before:text-sm before:bg-black before:text-white before:shadow-xl @sm:before:content-["screen:sm"] @md:before:content-["screen:md"] @lg:before:content-["screen:lg"] @xl:before:content-["screen:xl"] @2xl:before:content-["screen:2xl"] <sm:before:content-["screen:none"]'
  },
  theme: {
    extend: {
      screens: {
        sm: '640px',
        md: '768px',
        lg: '1024px',
        xl: '1280px',
        '2xl': '1536px'
      },
      colors: {
        gray: colors.coolGray,
        blue: colors.sky,
        red: colors.rose,
        pink: colors.fuchsia,
        primary: {
          100: '#ccf8e6',
          200: '#99f1cd',
          300: '#66eab4',
          400: '#33e39b',
          500: '#00dc82',
          600: '#00b068',
          700: '#00844e',
          800: '#005834',
          900: '#002c1a'
        }
      },
      fontFamily: {
        monospace: ['DMMono', 'monospace']
      },
      spacing: {
        128: '32rem',
        144: '36rem'
      },
      borderRadius: {
        '4xl': '2rem'
      },
      // typography: theme => ({
      //   // DEFAULT: {
      //   css: {
      //     color: '#333',
      //     // fontWeight: theme('fontWeight.semibold'),
      //     fontWeight: theme(),
      //     a: {
      //       color: '#3182ce',
      //       '&:hover': {
      //         color: '#2c5282'
      //       }
      //     }
      //   }
      //   // }
      // })
      typography: {
        dark: {
          css: {
            color: '#333',
            // fontWeight: theme('fontWeight.semibold'),
            fontWeight: 700,
            a: {
              color: '#3182ce',
              '&:hover': {
                color: '#2c5282'
              }
            }
          }
        }
      }
    }
  },
  plugins: [
    plugin(({ addUtilities }) => {
      const newUtilities = {
        '.skew-10deg': {
          transform: 'skewY(-10deg)'
        },
        '.skew-15deg': {
          transform: 'skewY(-15deg)'
        }
      }
      addUtilities(newUtilities)
    }),
    plugin(({ addComponents }) => {
      const buttons = {
        '.btn': {
          padding: '.5rem 1rem',
          borderRadius: '.25rem',
          fontWeight: '600'
        },
        '.btn-blue': {
          backgroundColor: '#3490dc',
          color: '#fff',
          '&:hover': {
            backgroundColor: '#2779bd'
          }
        },
        '.btn-red': {
          backgroundColor: '#e3342f',
          color: '#fff',
          '&:hover': {
            backgroundColor: '#cc1f1a'
          }
        }
      }
      addComponents(buttons)
    }),
    plugin(({ addDynamic, variants }) => {
      addDynamic('skew', ({ Utility, Style }) => {
        return Utility.handler
          .handleStatic(Style('skew'))
          .handleNumber(0, 360, 'int', number => `skewY(-${number}deg)`)
          .createProperty('transform')
      }, variants('skew'))
    }),
    filtersPlugin,
    formsPlugin,
    aspectRatioPlugin,
    lineClampPlugin,
    scrollbarPlugin,
    typographyPlugin({
      modifiers: ['DEFAULT', 'dark', 'sm', 'lg', 'red'],
      dark: true
    })
  ]
})
