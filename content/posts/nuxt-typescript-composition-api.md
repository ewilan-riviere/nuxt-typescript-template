---
title: "Nuxt: Typescript & Composition API"
subtitle: Setup Nuxt with Typescript and Composition API
description: How to setup Nuxt with Typescript and Composition API
---

## My configuration

```ts[nuxt.config.ts]
import type { NuxtConfig } from '@nuxt/types'

const config: NuxtConfig = {
  // ...
  buildModules: [
    ['@nuxt/typescript-build', // https://typescript.nuxtjs.org
      { typeCheck: false }
    ],
    '@nuxtjs/composition-api/module', // https://composition-api.nuxtjs.org
    // 'unplugin-vue2-script-setup/nuxt', // https://github.com/antfu/unplugin-vue2-script-setup
    'nuxt-windicss', // https://windicss.org/integrations/nuxt.html
    '@nuxtjs/svg-sprite', // https://github.com/nuxt-community/svg-sprite-module
    ['unplugin-auto-import/nuxt', // https://github.com/antfu/unplugin-auto-import
      {
        imports: ['@nuxtjs/composition-api']
      }
    ]
  ],
}

export default config
```

## **About `setup` with Composition API**

From [https://github.com/antfu/unplugin-vue2-script-setup](https://github.com/antfu/unplugin-vue2-script-setup)

> From v0.28.0 of `@nuxtjs/composition-api`, this plugin is included and enabled out-of-box.

```bash[npm]
npm i @nuxtjs/composition-api
```

```ts[nuxt.config.js]
export default {
  buildModules: [
    '@nuxtjs/composition-api/module',
  ],
  scriptSetup: { /* options */ },
}
```

> This module works for both Nuxt 2 and [Nuxt Vite](https://github.com/nuxt/vite)

Example: [`examples/nuxt`](https://github.com/antfu/unplugin-vue2-script-setup/tree/main/examples/nuxt)

### Component Meta

Note that `<script setup>` could co-exist with `<script>`, if you want to define component metadata like `layout` or `head` for Nuxt, you can do it this way:
  
```html
<script setup lang="ts">
// your script setup
</script>

<script lang="ts">
// the normal script
export default {
  layout: 'user',
  // ...other meta
}
</script>
```
  
### TypeScript

To use TypeScript with Nuxt, install the [`@nuxtjs/typescript-module`](https://typescript.nuxtjs.org/) but disable the type check:

```bash
npm i -D @nuxt/typescript-build vue-tsc
```

```ts
// nuxt.config.js
export default {
  buildModules: [
    ['@nuxt/typescript-build', { typeCheck: false }],
    '@nuxtjs/composition-api/module',
    'unplugin-vue2-script-setup/nuxt',
  ],
}
```

And then use [`vue-tsc`](https://github.com/johnsoncodehk/volar) to do the type check at build time:

```jsonc
// package.json
{
  "scripts": {
    "dev": "nuxt",
    "build": "vue-tsc --noEmit && nuxt build"
  }
}
```

## IDE

We recommend using [VS Code](https://code.visualstudio.com/) with [Volar](https://github.com/johnsoncodehk/volar) to get the best experience (You might want to disable Vetur if you have it).

When using Volar, you need to install `@vue/runtime-dom` as devDependencies to make it work on Vue 2.

```bash
npm i -D @vue/runtime-dom
```

[Learn more](https://github.com/johnsoncodehk/volar#using)

### Global Types

If the global types are missing for your IDE, update your `tsconfig.json` with:

```jsonc
{
  "compilerOptions": {
    "types": [
      "unplugin-vue2-script-setup/types"
    ]
  }
}
```

### Support Vue 2 template

Volar preferentially supports Vue 3. Vue 3 and Vue 2 template has some different. You need to set the `experimentalCompatMode` option to support Vue 2 template.

```jsonc
{
  "compilerOptions": {
    ...
  },
  "vueCompilerOptions": {
    "experimentalCompatMode": 2
  },
}
```

### ESLint

If you are using ESLint, you might get `@typescript-eslint/no-unused-vars` warning with `<script setup>`. You can disable it and add `noUnusedLocals: true` in your `tsconfig.json`, Volar will infer the real missing locals correctly for you.

## Configurations

**Ref Sugar (take 2)**

In v0.5.x, we shipped the **experimental** [Ref Sugar (take 2)](https://github.com/vuejs/rfcs/discussions/369) implementation based on Vue 3's [`@vue/ref-transform`](https://github.com/vuejs/vue-next/tree/master/packages/ref-transform) package. Notice the syntax is not settled yet and might be changed in the future updates. **Use at your own risk!**

To enabled it, pass the option:

```ts
ScriptSetup({
  refTransform: true
})
```

To get TypeScript support, update your `tsconfig.json` with:

```jsonc
{
  "compilerOptions": {
    "types": [
      "unplugin-vue2-script-setup/types",
      "unplugin-vue2-script-setup/ref-macros"
    ]
  }
}
```

## Recommendations

If you enjoy using `<script setup>`, you might also want to try [`unplugin-auto-import`](https://github.com/antfu/unplugin-auto-import) to improve the DX even further.
