---
title: nuxt/content
subtitle: With Typescript and Composition API
description: How to use nuxt/content with Typescript and Composition API
---

## Example

A basic template refer to `content/welcome.md` file

```vue[pages/index.vue]
<template>
  <layout-page>
    <div v-if="welcome" class="prose">
      {{ welcome.title }}
      <nuxt-content :document="welcome" />
    </div>
  </layout-page>
</template>
```

`<script>` with `setup` to fetch `.md` content from `$content` with *Nuxt context* (only available in `setup`)

```vue[pages/index.vue]
<script lang="ts">
import Vue from 'vue'
import CopyButton from '~/components/content/copy-button.vue'

export default defineComponent({
  setup (_props, context) {
    const ctx = useContext()
    const welcome = useAsync<Content>(() => {
      return ctx.$content('welcome')
        .fetch<Content>()
        .then(e => e.length ? e.find(true) : e) // prevent error with Typescript for `Content` type, `fetch` can return an `array`
    })

    return {
      welcome,
      content: ctx.$content // allow to use `content` outside of `setup`
    }
  },

  head: {},
  mounted () {
    // allow refresh cause of composition api
    // https://github.com/nuxt/content/issues/80
    if (process.env.NODE_ENV === 'development' && process.client) {
      window.$nuxt.$on('content:update', async () => {
        this.welcome = await this.content('welcome')
          .fetch<Content>()
          .then(e => e.length ? e.find(true) : e)
      })
    }
    // add copy button
    setTimeout(() => {
      const blocks = document.getElementsByClassName('nuxt-content-highlight')

      for (const block of blocks) {
        const Button = Vue.extend(CopyButton)
        const component = new Button().$mount()
        block.appendChild(component.$el)
      }
    }, 500)
  }
})
</script>
```

Here same feature but with Composition API `<script setup>`

```vue[pages/index.vue]
<script setup lang="ts">
import Vue from 'vue'
import CopyButton from '~/components/content/copy-button.vue'

interface Content {
  title: string,
  slug: string,
  description: string,
  dir: string,
  path: string,
  body: object,
  createdAt: Date,
  updatedAt: Date,
  extension: string,
  toc: { id:string, depth: number, text: string }[]
}

const ctx = useContext()
const welcome = useAsync<Content>(() => {
  return ctx.$content('welcome')
    .fetch<Content>()
    .then(e => e.length ? e.find(true) : e)
})

onMounted(() => {
  if (process.env.NODE_ENV === 'development' && process.client) {
    window.$nuxt.$on('content:update', async () => {
      welcome.value = await ctx.$content('welcome')
        .fetch<Content>()
        .then(e => e.length ? e.find(true) : e)
    })
  }
  setTimeout(() => {
    const blocks = document.getElementsByClassName('nuxt-content-highlight')

    for (const block of blocks) {
      const Button = Vue.extend(CopyButton)
      const component = new Button().$mount()
      block.appendChild(component.$el)
    }
  }, 500)
})
</script>
```
