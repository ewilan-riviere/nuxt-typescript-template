---
title: Nuxt TypeScript template
description: Learn how to use Typescript and Composition API with Nuxt.
---

Fork from [https://github.com/nuxt-community/typescript-template/tree/master](https://github.com/nuxt-community/typescript-template/tree/master)

> A [Nuxt.js](https://github.com/nuxt/nuxt.js) + [@nuxt/typescript](https://github.com/nuxt/typescript) starter project template.
>
>- Options API
>- Class API with with nuxt-property-decorator
>- Composition API (Experimental) with [@nuxt/composition-api](https://github.com/nuxt-community/composition-api)  
>
## **Setup**

With `Yarn`

```bash[yarn]
yarn
```

```bash[yarn]
yarn dev
```

With `NPM`

```bash[npm]
rm yarn.lock
npm i
```

```bash[npm]
npm run dev
```

Go to [http://localhost:3000](http://localhost:3000)
