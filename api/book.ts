import { stringify } from 'qs'
import { ApiFormated, ApiPaginateResponse, Book, BooksApiPaginateResponse, Pagination } from '~/types'

/**
 * Fetch Books
 */
// export const fetchBooks = async (): Promise<any> => {
//   console.log('books')

//   const todo = ref({})

//   return await fetch('https://gorest.co.in/public-api/todos/')
//     .then(response => response.json())
//     .then((json) => {
//       todo.value = json
//       return json
//     })
// }

export const fetchBooks = (perPage = 32): any => {
  const response = ref<BooksApiPaginateResponse>()
  const books = ref<Book[]>()
  const pagination = ref<Pagination>()
  const apiFormated = ref<ApiFormated>()
  const ctx = useContext()

  useAsync(async (): Promise<any> => {
    response.value = await ctx.$axios
      .$get(`/books?${stringify({ 'per-page': perPage })}`).then((e: BooksApiPaginateResponse) => {
        response.value = e
        books.value = e.data
        pagination.value = {
          pages: e.meta?.last_page,
          currentPage: e.meta?.current_page,
          perPage: e.meta?.per_page,
          total: e.meta?.total
        }
        apiFormated.value = {
          data: books.value,
          pagination: pagination.value
        }

        return e
      })
  })

  return response
}

// export const getBooks = (perPage = 32): ApiPaginateResponse => {
//   const response = ref<ApiPaginateResponse>()
//   const ctx = useContext()
//   //   const toExecute: any = []
//   useAsync<BooksApiPaginateResponse>(async () => {
//     response.value = await ctx.$axios.$get(`/booksa?${stringify({ 'per-page': perPage })}`).then((e: ApiPaginateResponse) => {
//       return e as ApiPaginateResponse
//     })
//   })

//   return response as unknown as ApiPaginateResponse
// }

export const getBooks = (perPage = 32): Book[] => {
  const books = ref<Book[]>()
  const ctx = useContext()

  useAsync(async () => {
    books.value = await ctx.$axios
      .$get(`/books?${stringify({ 'per-page': perPage })}`)
      .then((e) => {
        return e.data
      })
  })

  return books as Book[]
}

export const getBooksAlt = (perPage = 32): BooksApiPaginateResponse => {
  const response = ref<BooksApiPaginateResponse>()
  const ctx = useContext()

  try {
    response.value = useAsync<BooksApiPaginateResponse>(async () => {
      return await ctx.$axios
        .$get(`/books?${stringify({ 'per-page': perPage })}`)
        .then((e: BooksApiPaginateResponse) => e)
    })
  } catch (error) {

  }

  return response.value as unknown as BooksApiPaginateResponse
}
