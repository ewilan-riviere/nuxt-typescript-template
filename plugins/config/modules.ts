import pwaConfig from './pwa'
import metadata from './metadata'

const http = {
  // options
}
// Axios module configuration (https://go.nuxtjs.dev/config-axios)
const axios = {
  baseURL: process.env.API_URL,
  credentials: true,
  // https: true,
  headers: {
    common: {
      'X-Requested-With': 'XMLHttpRequest',
      'Access-Control-Allow-Origin': '*',
      Accept: 'application/json, text/plain, */*'
    }
  }
}
const pwa = {
  meta: pwaConfig.meta,
  manifest: pwaConfig.manifest
}
// const proxy = {
//   '/api': {
//     target: `${process.env.API_URL}`,
//     pathRewrite: { '^/': '/' },
//   },
// }

const auth = {
  strategies: {
    laravelSanctum: {
      provider: 'laravel/sanctum',
      url: process.env.API_URL,
      endpoints: {
        login: {
          url: '/login',
          method: 'post',
          propertyName: 'access_token'
        },
        logout: { url: '/logout', method: 'post' },
        user: { url: '/profile', method: 'get', propertyName: false }
      },
      tokenRequired: true,
      tokenType: 'Bearer',
      globalToken: true
    }
  },
  redirect: {
    login: '/sign-in',
    logout: '/sign-in',
    callback: '/',
    home: '/profile'
  },
  cookie: {
    options: {
      sameSite: 'lax',
      maxAge: 86400 // 24 hours
    }
  }
  // plugins: ['~/plugins/utils/auth.js']
}
// Content module configuration (https://go.nuxtjs.dev/config-content)
const content = {
  liveEdit: false,
  // https://github.com/remarkjs/remark/blob/main/doc/plugins.md#list-of-plugins
  markdown: {
    prism: {
      // https://github.com/PrismJS/prism-themes
      theme: 'assets/css/prism-night-owl.pcss'
    }
  }
}
const robots = {
  Disallow: metadata.settings.disallow.split(','),
  Sitemap: `${process.env.BASE_URL}/sitemap.xml`
}
const sitemap = {
  path: '/sitemap.xml',
  hostname: process.env.BASE_URL,
  cacheTime: 1000 * 60 * 15,
  gzip: true,
  exclude: metadata.settings.disallow.split(',')
  // sitemaps: sitemaps(),
}
// See https://github.com/markdown-it/markdown-it
const markdownit = {
  preset: 'default',
  linkify: true,
  breaks: true
  // use: ['markdown-it-div', 'markdown-it-attrs'],
}
const i18n = {
  locales: [
    {
      code: 'en',
      file: 'en.js',
      name: 'English'
    },
    {
      code: 'fr',
      file: 'fr.js',
      name: 'Français'
    }
  ],
  // lazy: true,
  defaultLocale: 'en',
  langDir: '~locales/',
  detectBrowserLanguage: {
    useCookie: true,
    cookieKey: 'i18n_redirected',
    redirectOn: 'root' // recommended
  }
}

const modules = {
  // http,
  axios,
  // pwa,
  // auth,
  content
  // robots,
  // sitemap,
  // markdownit,
  // i18n
}

export const nuxtLazyLoad = {
  directiveOnly: true,
  loadingClass: 'isLoading',
  loadedClass: 'isLoaded',
  appendClass: 'lazyLoad'
}

export const typescriptBuild = { typeCheck: false }
export const unpluginAutoImport = {
  imports: ['@nuxtjs/composition-api']
}

export default modules
