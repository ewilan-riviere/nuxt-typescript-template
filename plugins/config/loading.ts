import metadata from './metadata'

// https://nuxtjs.org/docs/configuration-glossary/configuration-loading/
const loading = {
  color: metadata.settings.color,
  height: '2px'
}

export default loading
