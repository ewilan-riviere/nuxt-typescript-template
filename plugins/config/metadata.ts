import windi from '../../windi.config'
import packageJson from '../../package.json'

export interface MetaHead {
  hid?: string,
  charset?: string,
  name?: string,
  property?: string,
  content?: string
}

const color: string = windi.theme.extend.colors.primary[600]

const app = 'Nuxt Typescript template'
const author = packageJson.author
const description: string = packageJson.description
const license: string = packageJson.license

const twitterLink = '@nuxt_js'

const metadata = {
  settings: {
    robots: 'index, follow',
    disallow: '',
    color,
    locale: 'en_US',
    lang: 'en'
  },
  website: {
    title: app,
    titleTemplate: `%s · ${app}`,
    description,
    rating: 'general',
    keywords: [],
    author,
    publisher: app,
    copyright: license,
    language: 'english',
    designer: author
  },
  og: {
    type: 'website',
    siteName: app
  },
  twitter: {
    creator: twitterLink,
    site: twitterLink,
    url: `https://twitter.com/${twitterLink.replace('@', '')}`
  }
}

export default metadata
